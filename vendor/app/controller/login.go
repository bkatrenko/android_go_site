package controller

import (
	"fmt"
	"log"
	"net/http"

	"app/model"
	"app/shared/passhash"
	"app/shared/session"
	"app/shared/view"

	"github.com/gorilla/sessions"
	"github.com/josephspurrier/csrfbanana"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2"
	"io/ioutil"
	"encoding/json"
	"app/shared/email"
	"time"
	"strconv"
)

const (
	// Name of the session variable that tracks login attempts
	sessLoginAttempt = "login_attempt"
)

var (
	googleOauthConfig = &oauth2.Config {
		RedirectURL:    "http://bkatrenko.scw.shpp.me/GoogleCallback",
		ClientID:     "425649399468-g8da0ikem6giisi451j0qco50l24pgif.apps.googleusercontent.com",
		ClientSecret: "6i29umfGhHd0gIaACaPkFgxB",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile",
				       "https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	// Some random string, random for each request
	oauthStateString = "random"
)

// loginAttempt increments the number of login attempts in sessions variable
func loginAttempt(sess *sessions.Session) {
	// Log the attempt
	if sess.Values[sessLoginAttempt] == nil {
		sess.Values[sessLoginAttempt] = 1
	} else {
		sess.Values[sessLoginAttempt] = sess.Values[sessLoginAttempt].(int) + 1
	}
}

// LoginGET displays the login page
func LoginGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	// Display the view
	v := view.New(r)
	v.Name = "login/login"
	v.Vars["token"] = csrfbanana.Token(w, r, sess)
	// Refill any form fields
	view.Repopulate([]string{"email"}, r.Form, v.Vars)
	v.Render(w)
}

// LoginPOST handles the login form submission
func LoginPOST(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	 //Prevent brute force login attempts by not hitting MySQL and pretending like it was invalid :-)
	if sess.Values[sessLoginAttempt] != nil && sess.Values[sessLoginAttempt].(int) >= 50 {
		log.Println("Brute force login prevented")
		sess.AddFlash(view.Flash{"Ты не пройдешь!", view.FlashNotice})
		sess.Save(r, w)
		LoginGET(w, r)
		return
	}

	// Validate with required fields
	if validate, missingField := view.Validate(r, []string{"email", "password"}); !validate {
		sess.AddFlash(view.Flash{"Вы забыли: " + missingField, view.FlashError})
		sess.Save(r, w)
		LoginGET(w, r)
		return
	}

	// Form values
	email := r.FormValue("email")
	password := r.FormValue("password")

	// Get database result
	result, err := model.UserByEmail(email)

	// Determine if user exists
	if err == model.ErrNoResult {
		loginAttempt(sess)
		sess.AddFlash(view.Flash{"Андроид не знает этого пароля: " + fmt.Sprintf("%v", sess.Values[sessLoginAttempt]), view.FlashWarning})
		sess.Save(r, w)
	} else if err != nil {
		// Display error message
		log.Println(err)
		sess.AddFlash(view.Flash{"Упс, что-то пошло не так.", view.FlashError})
		sess.Save(r, w)
	} else if passhash.MatchString(result.Password, password) {
		if result.StatusID != 1 {
			// User inactive and display inactive message
			sess.AddFlash(view.Flash{"Ваш андроид не отвечает.", view.FlashNotice})
			sess.Save(r, w)
		} else {
			// Login successfully
			session.Empty(sess)
			sess.AddFlash(view.Flash{"Добро пожаловать, Андроидщик!", view.FlashSuccess})
			sess.Values["id"] = result.UserID()
			sess.Values["email"] = email
			sess.Values["first_name"] = result.FirstName
			sess.Save(r, w)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
	} else {
		loginAttempt(sess)
		sess.AddFlash(view.Flash{"Вы пытались обмануть Андроида: " + fmt.Sprintf("%v", sess.Values[sessLoginAttempt]), view.FlashWarning})
		sess.Save(r, w)
	}

	// Show the login page again
	LoginGET(w, r)
}

// LogoutGET clears the session and logs the user out
func LogoutGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	// If user is authenticated
	if sess.Values["id"] != nil {
		session.Empty(sess)
		sess.AddFlash(view.Flash{"Пока)", view.FlashNotice})
		sess.Save(r, w)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func HandleGoogleLogin(w http.ResponseWriter, r *http.Request) {
	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func HandleGoogleCallback(w http.ResponseWriter, r *http.Request) {
	sess := session.Instance(r)

	state := r.FormValue("state")
	if state != oauthStateString {
		failGoogleLoginActions(nil, w, r, sess)
		return
	}

	code := r.FormValue("code")
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)

	if err != nil {
		failGoogleLoginActions(err, w, r, sess)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)

	if err != nil {
		failGoogleLoginActions(err, w, r, sess)
		return
	}

	defer response.Body.Close()

	contents, err := ioutil.ReadAll(response.Body)

	var googleUser model.GoogleUser
	err = json.Unmarshal(contents, &googleUser)

	if err != nil {
		log.Println("error when unmarhsall new google user")
		http.Error(w, "", http.StatusInternalServerError)
	}

	if googleUser.Email == "" {
		log.Println("user is void")
		http.Error(w, "", http.StatusInternalServerError)
	}

	log.Println("save new google user")
	SaveNewGoogleUser(w,r, googleUser.Email, googleUser.Given_name, googleUser.Family_name)
	signInFromGoogleSuccess(w,r, sess, &googleUser)
	//fmt.Fprintf(w, "Content: %s\n", contents)
}

func addFailLoginFlashToSession(w http.ResponseWriter, r *http.Request, sess *sessions.Session){
	session.Empty(sess)
	sess.AddFlash(view.Flash{"Что-то пошло не так", view.FlashSuccess})
	sess.Save(r, w)
}

func failGoogleLoginActions(err error, w http.ResponseWriter, r *http.Request, sess *sessions.Session) {
	if err != nil {
		log.Println("Code exchange failed with '%s'\n", err)
	}

	addFailLoginFlashToSession(w, r, sess)
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func signInFromGoogleSuccess(w http.ResponseWriter, r *http.Request, sess *sessions.Session, user *model.GoogleUser){
	session.Empty(sess)

	result, err := model.UserByEmail(user.Email)

	if err != nil{
		log.Println("user not found when trying login with google")
		http.Error(w, "user not found", http.StatusInternalServerError)
	}

	sess.AddFlash(view.Flash{"Добро пожаловать, Андроидщик!", view.FlashSuccess})
	sess.Values["id"] = result.UserID()
	sess.Values["email"] = user.Email
	sess.Values["first_name"] = result.FirstName
	sess.Save(r, w)
	http.Redirect(w, r, "/", http.StatusFound)
	log.Println("sign in with google success")
}

// AboutGET displays the About page
func ForgotPasswordGet(w http.ResponseWriter, r *http.Request) {
	// Display the view
	sess := session.Instance(r)

	v := view.New(r)
	v.Name = "login/forgot_password"
	v.Vars["token"] = csrfbanana.Token(w, r, sess)
	v.Render(w)
}

// AboutGET displays the About page
func ForgotPasswordPost(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	//Prevent brute force login attempts by not hitting MySQL and pretending like it was invalid :-)
	if sess.Values[sessLoginAttempt] != nil && sess.Values[sessLoginAttempt].(int) >= 1000 {
		log.Println("Brute force login prevented")
		w.WriteHeader(http.StatusLocked)
		w.Write([]byte("Брутфорс не пройдет"))
		return
	}

	// Validate with required fields
	if validate, missingField := view.Validate(r, []string{"email"}); !validate {
		log.Println("Bad request")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Вы забыли: " + missingField))
		return
	}

	// Form values
	userEmail := r.FormValue("email")

	// Get database result
	user, err := model.UserByEmail(userEmail)

	// Determine if user exists
	if err == model.ErrNoResult {
		log.Println("not found user")
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Андроид не знает такого пользователя!"))
		return

	} else if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("У андроида произошла ошибка..."))
		return
	}

	err = model.UserUpdateWithBoltDb(&user)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("У андроида произошла ошибка..."))
		return
	}

	email.SendEmail(user.Email, "Смена пароля на сайте bkatrenko.scw.shpp.me", "Ваш новый пароль - " +
		strconv.Itoa(time.Now().Nanosecond()))

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Новый пароль выслан на имейл " + user.Email))
}