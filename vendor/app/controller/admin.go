package controller

import (
	"net/http"
	"app/shared/view"
	"app/model"
	"log"
	"app/shared/session"
	"fmt"
	"github.com/josephspurrier/csrfbanana"
	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/context"
)

func ShowUsersGet(w http.ResponseWriter, r *http.Request) {
	v := view.New(r)
	v.Name = "admin/admin"

	users, err := model.GetAllUsersList()
	if err != nil {
		log.Println(err)
		users = []model.User{}
	}

	v.Vars["users"] = users
	v.Render(w)
}

func LessonsReadGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)
	lessons, err := model.GetAllLessons()
	if err != nil {
		log.Println(err)
		lessons = []model.Lesson{}
	}

	for k, v := range lessons {
		if len(v.Content) > 300 {
			lessons[k].Content = v.Content[:300] + "..."
		}
	}

	// Display the view
	v := view.New(r)
	v.Name = "admin/lessons_list"
	v.Vars["first_name"] = sess.Values["first_name"]
	v.Vars["lessons"] = lessons
	v.Render(w)
}

func AddNewLessonGET(w http.ResponseWriter, r *http.Request) {
	sess := session.Instance(r)
	v := view.New(r)
	v.Name = "admin/add_lesson_template"
	v.Vars["token"] = csrfbanana.Token(w, r, sess)
	v.Render(w)
}

func AddNewLessonPOST(w http.ResponseWriter, r *http.Request) {
	sess := session.Instance(r)

	// Validate with required fields
	if validate, missingField := view.Validate(r, []string{"title", "body"}); !validate {
		sess.AddFlash(view.Flash{"Вы забыли: " + missingField, view.FlashError})
		sess.Save(r, w)
		AddNewLessonGET(w, r)
		return
	}

	// Form values
	title := r.FormValue("title")
	body := r.FormValue("body")

	userID := fmt.Sprintf("%s", sess.Values["id"])

	// Get database result
	err := model.LessonCreate(title, body, userID)
	// Will only error if there is a problem with the query
	if err != nil {
		log.Println(err)
		sess.AddFlash(view.Flash{"An error occurred on the server. Please try again later.", view.FlashError})
		sess.Save(r, w)
	} else {
		sess.AddFlash(view.Flash{"Ура, создано новое занятие!", view.FlashSuccess})
		sess.Save(r, w)
		http.Redirect(w, r, "/lesson/lessons_list", http.StatusFound)
		return
	}

	// Display the same page
	AddNewLessonGET(w, r)
}

// NotepadUpdateGET displays the note update page
func UpdateLessonGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)
	// Get the note id
	var params httprouter.Params

	params = context.Get(r, "params").(httprouter.Params)
	noteID := params.ByName("id")

	userID := fmt.Sprintf("%s", sess.Values["id"])

	// Get the note
	lesson, err := model.LessonByID(userID, noteID)

	log.Println("get lesson by id = " + lesson.Title + " " + lesson.Content)

	if err != nil { // If the note doesn't exist
		log.Println(err)
		sess.AddFlash(view.Flash{"An error occurred on the server. Please try again later.", view.FlashError})
		sess.Save(r, w)
		http.Redirect(w, r, "/admin", http.StatusFound)
		return
	}

	// Display the view
	v := view.New(r)
	v.Name = "admin/update_lesson_template"
	v.Vars["token"] = csrfbanana.Token(w, r, sess)
	v.Vars["title"] = lesson.Title
	v.Vars["body"] = lesson.Content
	v.Vars["id"] = noteID
	v.Render(w)
}

// NotepadUpdatePOST handles the note update form submission
func UpdateLessonPOST(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	// Validate with required fields
	if validate, missingField := view.Validate(r, []string{"title", "body"}); !validate {
		sess.AddFlash(view.Flash{"Поле не заполнено: " + missingField, view.FlashError})
		sess.Save(r, w)
		NotepadUpdateGET(w, r)
		return
	}

	// Form values
	title := r.FormValue("title")
	body := r.FormValue("body")

	userID := fmt.Sprintf("%s", sess.Values["id"])

	var params httprouter.Params
	params = context.Get(r, "params").(httprouter.Params)
	noteID := params.ByName("id")

	// Get database result
	err := model.LessonUpdate(title, body, userID, noteID)
	// Will only error if there is a problem with the query
	if err != nil {
		log.Println(err)
		sess.AddFlash(view.Flash{"Еще разок.", view.FlashError})
		sess.Save(r, w)
	} else {
		sess.AddFlash(view.Flash{"Заметка обновлена!", view.FlashSuccess})
		sess.Save(r, w)
		http.Redirect(w, r, "/lesson/lessons_list", http.StatusFound)
		return
	}

	// Display the same page
	UpdateLessonGET(w, r)
}

func LessonDeleteGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	sess := session.Instance(r)

	userID := fmt.Sprintf("%s", sess.Values["id"])

	var params httprouter.Params
	params = context.Get(r, "params").(httprouter.Params)
	noteID := params.ByName("id")

	// Get database result
	err := model.LessonDelete(userID, noteID)
	// Will only error if there is a problem with the query
	if err != nil {
		log.Println(err)
		sess.AddFlash(view.Flash{"Нужно попробовать еще разок.", view.FlashError})
		sess.Save(r, w)
	} else {
		sess.AddFlash(view.Flash{"Заметка удалена (но не точно)", view.FlashSuccess})
		sess.Save(r, w)
	}

	http.Redirect(w, r, "/lesson/lessons_list", http.StatusFound)
	return
}
