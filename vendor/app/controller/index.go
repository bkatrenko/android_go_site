package controller

import (
	"net/http"

	"app/shared/session"
	"app/shared/view"
	"app/model"
	"log"
)

// IndexGET displays the home page
func IndexGET(w http.ResponseWriter, r *http.Request) {
	// Get session
	session := session.Instance(r)

	if session.Values["id"] != nil {
		// Display the view
		v := view.New(r)

		lessons, err := model.GetAllLessons()
		if err != nil {
			log.Println("lessons not found")
			//http.Error(w, "error while getting lessong" , http.StatusInternalServerError)
		}

		v.Name = "index/auth"

		v.Vars["lessons"] = lessons
		v.Vars["first_name"] = session.Values["first_name"]

		v.Render(w)
	} else {
		// Display the view
		v := view.New(r)
		v.Name = "index/anon"
		v.Render(w)
		return
	}
}


