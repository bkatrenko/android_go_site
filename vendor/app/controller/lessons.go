package controller

import (
	"net/http"
	"app/shared/session"
	"app/shared/view"
	"github.com/josephspurrier/csrfbanana"
	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/context"
	"app/model"
	"github.com/russross/blackfriday"
	"html/template"
)

func GetLessonById(w http.ResponseWriter, r *http.Request){
	// Get session
	sess := session.Instance(r)
	//userID := fmt.Sprintf("%s", sess.Values["id"])

	params := context.Get(r, "params").(httprouter.Params)
	noteID := params.ByName("id")

	user, err := model.UserByEmail("katrenko.bogdan@gmail.com")
	if err != nil{
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Display the view
	lesson, err := model.LessonByID(user.UserID(), noteID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	v := view.New(r)
	v.Name = "/lessons/lesson"
	v.Vars["token"] = csrfbanana.Token(w, r, sess)
	v.Vars["LessonContent"] = template.HTML(blackfriday.MarkdownCommon([]byte(lesson.Content)))
	v.Vars["LessonTitle"] = lesson.Title
	v.Render(w)
}









