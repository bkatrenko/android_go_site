package model


import (
	"time"
	"app/shared/database"
	"gopkg.in/mgo.v2/bson"
	"encoding/json"
	"log"
	"github.com/boltdb/bolt"
)

// *****************************************************************************
// Note
// *****************************************************************************

// Note table contains the information for each note
type Lesson struct {
	ObjectID  bson.ObjectId `bson:"_id"`
	ID        uint32        `db:"id" bson:"id,omitempty"` // Don't use Id, use NoteID() instead for consistency with MongoDB
	Title     string	`db:"title" bson:"title"`
	Content   string        `db:"content" bson:"content"`
	UserID    bson.ObjectId `bson:"user_id"`
	UID       uint32        `db:"user_id" bson:"userid,omitempty"`
	CreatedAt time.Time     `db:"created_at" bson:"created_at"`
	UpdatedAt time.Time     `db:"updated_at" bson:"updated_at"`
	Deleted   uint8         `db:"deleted" bson:"deleted"`
}

// NoteID returns the note id
func (u *Lesson) LessonID() string {
	r := ""
	r = u.ObjectID.Hex()

	return r
}

// NotesByUserID gets all notes for a user
func GetAllLessons() ([]Lesson, error) {
	var err error
	var result []Lesson

		// View retrieves a record set in Bolt
		err = database.BoltDB.View(func(tx *bolt.Tx) error {

			b := tx.Bucket([]byte("lessons"))
			if b == nil {
				return bolt.ErrBucketNotFound
			}

			// Get the iterator
			c := b.Cursor()

			for k, v := c.First(); k != nil; k, v = c.Next() {
				var single Lesson

				// Decode the record
				err := json.Unmarshal(v, &single)
				if err != nil {
					log.Println(err)
					continue
				}

				result = append(result, single)
			}

			return nil
		})

	return result, standardizeError(err)
}

func LessonByID(userID string, noteID string) (Lesson, error) {
	var err error
	result := Lesson{}

	err = database.View("lessons", userID+noteID, &result)
	if err != nil {
		err = ErrNoResult
	}

	//if result.UserID != bson.ObjectIdHex(userID) {
	//	result = Lesson{}
	//		err = ErrUnauthorized
	//}

	return result, standardizeError(err)
}

func LessonCreate(title string, content string, userID string) error {
	var err error

	now := time.Now()

	note := &Lesson{
		ObjectID:  bson.NewObjectId(),
		Title:     title,
		Content:   content,
		UserID:    bson.ObjectIdHex(userID),
		CreatedAt: now,
		UpdatedAt: now,
		Deleted:   0,
	}

	err = database.Update("lessons", userID+note.ObjectID.Hex(), &note)

	return standardizeError(err)
}

func LessonUpdate(title string, content string, userID string, noteID string) error {
	var err error
	var note Lesson
	now := time.Now()

	note, err = LessonByID(userID, noteID)
	if err == nil {
		// Confirm the owner is attempting to modify the note
		if note.UserID.Hex() == userID {
			note.UpdatedAt = now
			note.Title = title
			note.Content = content
			err = database.Update("lessons", userID+note.ObjectID.Hex(), &note)
		} else {
			err = ErrUnauthorized
		}
	}

	return standardizeError(err)
}

func LessonDelete(userID string, noteID string) error {
	var err error

		var note Lesson
		note, err = LessonByID(userID, noteID)
		if err == nil {
			// Confirm the owner is attempting to modify the note
			if note.UserID.Hex() == userID {
				err = database.Delete("lessons", userID+note.ObjectID.Hex())
			} else {
				err = ErrUnauthorized
			}
		}


	return standardizeError(err)
}
