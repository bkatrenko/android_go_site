# Simple portal for Sh++ Android students

Minimalistic and "just working" site for classes on android development. Basic Features:
> - Registration and login of students
> - Ability to use multiple databases (BoltDB, SQLite)
> - Download lessons from MD to HTML
> - Cute FLASH notifiations for users

I recommend to install in a pair of NGINX.