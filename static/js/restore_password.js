/**
 * Created by bogdan on 4/6/17.
 */
$(document).ready(function() {
    $(".progress").hide();
});

function call() {
    $(".progress").show();
    var msg   = $('#restore_form').serialize();
    $.ajax({
        type: 'POST',
        url: '/restore_password',
        data: msg,

        success: function(data) {
            alert(data);
            console.log(data);
            $(".progress").hide();
        },

        error:  function(xhr, str){
            showFlash({cssclass:"alert-danger", message: xhr.responseText});
            $(".progress").hide();
        }
    });
}
