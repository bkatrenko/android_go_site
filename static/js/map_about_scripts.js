/**
 * Created by bogdan on 4/14/17.
 */
function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 48.504794, lng: 32.261080},
        scrollwheel: false,
        zoom: 8
    });

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: {lat: 48.504794, lng: 32.261080}
    });

    var marker = new google.maps.Marker({
        position: {lat: 48.504794, lng: 32.261080},
        map: map,
        title: 'Hello World!'
    });
}
